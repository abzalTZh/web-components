import { SectionCreater } from './join-us-section.js';
import { SectionCreater2 } from './community-section.js';
import { validate } from './email-validator';
import { sendSubscribe, unsubscribeUser} from './subscribe';
import { WebsiteSection } from './website-section.js';
import '../styles/style.css';
import '../styles/normalize.css';

const joinSection = new SectionCreater;
const commSection = new SectionCreater2;

window.addEventListener('load', joinSection.create('standard'), false);
window.addEventListener('load', commSection.create(), false);
window.addEventListener('load', () => {
  if (validate(email)) {
    emailInput.style.display = 'none';
    subscribeBtn.textContent = 'Unsubscribe';
  }
}, false);

const form = document.querySelector('form.app-form');
const emailInput = document.querySelector('input.app-form__email-input');
const email = localStorage.getItem('email');
const subscribeBtn =
  document.querySelector('button.app-section__button.app-section__button--subscribe');

form.addEventListener('submit', (e) => {
  e.preventDefault();

  if (validate(emailInput.value)) {
    localStorage.setItem('email', emailInput.value);
    if (emailInput.style.display === 'none') {
      emailInput.style.display = 'block';
      subscribeBtn.textContent = 'Subscribe';
      unsubscribeUser();
      localStorage.removeItem('email');
      disableBtn();
      setTimeout(enableBtn, '1000')
    } else {
      emailInput.style.display = 'none';
      subscribeBtn.textContent = 'Unsubscribe';
      subscribeBtn.style.marginLeft = 0;
      sendSubscribe(emailInput.value);
      disableBtn();
      setTimeout(enableBtn, '1000')
    }
  }
}, false);

email ? emailInput.setAttribute('value', email) : emailInput.setAttribute('value', '');

const disableBtn = () => {
  subscribeBtn.setAttribute('disabled', 'disabled')
  subscribeBtn.style.opacity = '0.5'
}
const enableBtn = () => {
  subscribeBtn.removeAttribute('disabled')
  subscribeBtn.style.opacity = '1'
}

customElements.define('website-section', WebsiteSection);