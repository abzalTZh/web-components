export class WebsiteSection extends HTMLElement {
    constructor() {
        super();

        const tmpl = document.querySelector('#website-section-tmpl').content;

        const shadow = this.attachShadow( { mode: "open" } );

        const title = document.createElement('h1');
        title.setAttribute('class', 'app-title');
        const subtitle = document.createElement('h2');
        subtitle.setAttribute('class', 'app-subtitle');

        const titleText = this.getAttribute('title');
        title.innerHTML = titleText;
        const subtitleText = this.getAttribute('desc');
        subtitle.innerHTML = subtitleText;

        const linkElem = document.createElement("link");
        linkElem.setAttribute("rel", "stylesheet");
        linkElem.setAttribute("href", "./main.css");

        if(this.getAttribute('slotPos') === 'before') {
            shadow.appendChild(tmpl.cloneNode(true));
            shadow.appendChild(linkElem);
            shadow.append(title);
            shadow.appendChild(subtitle);
        }

        if(this.getAttribute('slotPos') === 'after') {
            shadow.appendChild(linkElem);
            shadow.append(title);
            shadow.appendChild(subtitle);
            shadow.appendChild(tmpl.cloneNode(true));
        }

        if(this.getAttribute('slotPos') === 'between') {
            shadow.appendChild(linkElem);
            shadow.append(title);
            shadow.appendChild(tmpl.cloneNode(true));
            shadow.appendChild(subtitle);
        }
    }
}