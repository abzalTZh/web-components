export class SectionCreater {
  create (type) {
    if (type === 'standard') {
      const headerContent = document.createTextNode('Join Our Program');
      const formButtonText = document.createTextNode('Subscribe');

      return new Section(headerContent, formButtonText).constructSection();
    }

    if (type === 'advanced') {
      const headerContent = document.createTextNode('Join Our Advanced Program');
      const formButtonText = document.createTextNode('Subscribe to Advanced Program');

      return new Section(headerContent, formButtonText).constructSection();
    }
  }
}

class Section {
    constructor (headerContent, formButtonText) {
        this.headerContent = headerContent;
        this.formButtonText = formButtonText;
    }

    constructSection () {
        const learnMore = document.querySelector('website-section.app-section.app-section--image-culture');

        const joinSection = document.createElement('website-section');
        joinSection.className = 'app-section app-section--image-join';
        learnMore.parentNode.insertBefore(joinSection, learnMore.nextSibling);

        const sectionHeader = this.headerContent;
        joinSection.setAttribute('title', sectionHeader);
    
        const sectionSubtitle = 'Sed do eiusmod tempor incididunt <br />ut labore et dolore magna aliqua.';
        joinSection.setAttribute('desc', sectionSubtitle);
    
        const formContainer = document.createElement('form');
        formContainer.className = 'app-form';
        formContainer.setAttribute('slot', 'add-content');
        const emailForm = document.createElement('input');
        emailForm.className = 'app-form__email-input';
        emailForm.setAttribute('type', 'email');
        emailForm.setAttribute('name', 'email');
        emailForm.setAttribute('placeholder', 'Email');
    
        joinSection.appendChild(formContainer);
        formContainer.appendChild(emailForm);
    
        const formButton = document.createElement('button');
        formButton.className = 'app-section__button app-section__button--subscribe';
        formButton.appendChild(this.formButtonText);
    
        formContainer.appendChild(formButton);

        joinSection.setAttribute('slotPos', 'after');
    }
}