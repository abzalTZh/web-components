import { validate } from "./email-validator";

export const sendSubscribe = async (email) => {
    const isValidEmail = validate(email)
    if (isValidEmail === true) {
        await sendData(email);
    }
  }
  
  export const sendHttpRequest = async (method, url, data) => {
    return await fetch(url, {
      method: method,
      body: JSON.stringify(data),
      headers: data
        ? {
            'Content-Type': 'application/json'
          }
        : {}
    }).then(response => {
      if (response.status >= 400) {
        return response.json().then(errResData => {
          const error = new Error('Something went wrong!')
          error.data = errResData
          throw error
        })
      }
      return response.json()
    })
  }
  
  const sendData = async (email) => {
    await sendHttpRequest('POST', 'http://localhost:3000/subscribe', {
      email: email
    }).then(responseData => {
      return responseData
    }).catch(err => {
      console.log(err, err.data)
      window.alert(err.data.error)
    })
  }
  
  export const unsubscribeUser = async () => {
    await fetch('http://localhost:3000/unsubscribe', { method: 'POST' }).then(response => { console.log(response.status) })
  }